import os

current_Dir = os.path.dirname(os.path.realpath('__file__'))

def consolidateLogs():
    print "[vat-log] consolidateLogs def got exicuted..."
    pathToTempDir = 'temp/'
    pathToTempDir= os.path.join(current_Dir, 'temp/')
    allUploadedLogFies= os.listdir(pathToTempDir)
    newFileName=str(pathToTempDir)+"dfwpktlogsVAT.log"
    print "[vat-log] Content found in temp folder", allUploadedLogFies
    #print "length of allUploadedLogFies is:", len(allUploadedLogFies)
    if len(allUploadedLogFies)==0:
        print "[vat-log] No log file had been uploaded to temp folder!"
    elif len(allUploadedLogFies)==1:
        if "dfwpktlogs" in allUploadedLogFies[0]:
            print "[vat-log] One log file found. Renaming it to 'dfwpktlogsVAT.log'"
            fileName = allUploadedLogFies[0]
            file_path = os.path.join(pathToTempDir, fileName)
            os.rename(file_path,newFileName)
            #copyfile(file_path,newFileName)
        else:
            print "[vat-log] No .log file had been uploaded to temp folder!"
    else:
        tempFileCount=0
        with open(newFileName,'w') as outfile:
            for fname in allUploadedLogFies:
                #print "fname is:", fname
                if "dfwpktlogs" in fname:
                    tempFileCount=tempFileCount+1
                    if tempFileCount > 1:
                        outfile.write('\n')
                    fileWithPath = os.path.join(pathToTempDir, fname)
                    print "[vat-log] Appending File:", fname
                    with open(fileWithPath) as infile:
                        outfile.write(infile.read())
    #return newFileName

# -------------------------------------------------- #
#  Main function - calling consolidateLogs function 
# -------------------------------------------------- #
if __name__ == '__main__':
    consolidateLogs()