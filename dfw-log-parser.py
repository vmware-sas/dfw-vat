# *-----------------------------------------------* #
# ************************************************* #
# VMware NSX dfw Log Summery by @thisispuneet       #
# This script parse dfw log data into dic structure #
# and use analytical search on that dic             #
# We display final data and graphs on an excel file #
# ************************************************* #
# *-----------------------------------------------* #
#                 Version: 1.1.3                    #
# *-----------------------------------------------* #

# Following are the list of all python packages used by this script
# You will need to Install "re" and "xlsxwriter" on local machine/env 
# - from where you running the script. 
import sys
import re
import xlsxwriter
import operator
import os
from os import path
# Importing Socket to get host name
import socket
import multiprocessing
from multiprocessing import Manager
manager = Manager()

current_Dir = path.dirname(path.realpath('__file__'))

# Following are the path to input file (NSX dfw logs)
# and output file (.xlsx format). 
####inputLogFile = 'dfwpktlogs-vat.log'
if __name__ == '__main__':
    outputExcelFile = 'dfwpktlogsVAT.xlsx'
else:
    outputExcelFile = path.join(current_Dir, 'temp/dfwpktlogsVAT.xlsx')

# Following is the list where you may specify an IP.
# Script will search for this IP in the provided log file,
# and create a seperate worksheet for this IP. W/ all the related details.

# If no IP is specified here, script will ask for any specific IP to search 
# when script is been executed.
#listOfIPtoSearch=['10.225.18.20']

# Following are the multiple dictionaries where we'll be saving are parsed data from dfw log files.
dicOfIPs={} # Dictionary of Source IP and Dictionary of Destiantion IP.
dicOfDestinationIP={}
dicOfINPorts={}
dicOfOUTPorts={}
temp_dicOfINPorts={}
temp_dicOfOUTPorts={}
dicOfINDestinationIP={}
dicOfOUTDestinationIP={}

# Following are the two lists where we'll be saving the list of top IN and OUT ports.
listOfTopINPorts=[]
listOfTopOUTPorts=[]

# Following are the dict where we'll be saving the list of DFW Rule ID used.
dicOfDFWRuleid={}
dicOfINDFWRuleid={}
dicOfOUTDFWRuleid={}

# Following are the dict used to save Domain name for Destination and Source IP.
tempDicOfAllTrafficIP=manager.dict()

# Following are the multiple global variable which will help us keep the count of different traffic options.
# These variables will also be used to dynamically display the correct data in excel sheet.
portNumAssociationDic = {22:'SSH/SFTP/portForwarding', 53:'DNS', 80: 'HTTP', 135:'DCE', 137:'Name Service', 445:'Active Directory', 1433:'SQL', 1434:'SQL', 1434:'MSSQL', 1645:'RADIUS', 5355:'LLMNR-WinServer', 5985:'Win PowerShell'}

def getHostName(hostIP, tempDicOfAllTrafficIP):
    try:
        hostName = socket.gethostbyaddr(hostIP)[0]
        tempDicOfAllTrafficIP[hostIP] = hostName
    except Exception as e:
        ##print "Moving on to next process as this one errored out ->", e
        tempDicOfAllTrafficIP[hostIP] = None
        pass

# Following function deletes all the files from temp directory
# Thisfunction is exicuted once user downloads the excelfile
# For online/website platform only, not used if you running the script locally.
def cleanTempDir():
    pathToTempDir = 'temp/'
    pathToTempDir= path.join(current_Dir, 'temp/')
    print "pathToTempDir is:", pathToTempDir
    for the_file in os.listdir(pathToTempDir):
        file_path = os.path.join(pathToTempDir, the_file)
        try:
            if os.path.isfile(file_path):
                print "Deleting file:",file_path
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

# Following is a small IP validation function.
# We use this function to validate the input IP (IP that user enter to be searched within log. - 'listOfIPtoSearch')
def validateIPs(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


# Main Function dfw_vat. It's get exicuted by default when this .py file get called.
# it Returns the report as weorkbook
def dfw_vat(inputLogFile="dfwpktlogsVAT.log", ipToSearch=None, searchHostName=False):
    inputLogFile = str(inputLogFile)
    print "[vat-log] dfw_vat def got exicuted."
    if ipToSearch == '':
        ipToSearch = None
    pathToFile = 'temp/'+inputLogFile
    uploadedFilePath= path.join(current_Dir, pathToFile)
    print "uploadedFilePath is:", uploadedFilePath
    print "ipToSearch input is:", ipToSearch
    listOfIPtoSearch=[]
    if ipToSearch != None:
        listOfIPtoSearch.append(ipToSearch)
    (inTrafficCount, outTrafficCount, udpProtoCount, tcpProtoCount, proto1ProtoCount, inUDPProtoCount, inTCPProtoCount, inProto1ProtoCount, 
        numberOfINPortsForGraph, totalINMisCount, outUDPProtoCount, outTCPProtoCount, outProto1ProtoCount, numberOfOUTPortsForGraph, totalOUTMisCount) = (0,)*15

    # ******************************************************** #
    # Lets start the code here - First we'll open the log file #
    # and check if its empty? If not we'll reed the # of lines #
    # ******************************************************** #
    # Using with construct, as it close the file automatically.
    with open(uploadedFilePath) as f:
        logData = f.readlines()
        if len(logData)==0:
            sys.exit("Log file is empty!")
        else:
            numberOfLogs = len(logData)
            startLogDate = logData[0][:19]
            endLogDate = logData[-1][:19]
            print "[vat-log] Number of lines to parse in this log file are:", numberOfLogs
            print "[vat-log] Start Log Date is:", startLogDate
            print "[vat-log] End Log Date is:", endLogDate

    # ******************************************************* #
    # Lets check if more than one log file had been uploaded? #
    # If yes we'll combined them all in one .log file         #
    # ******************************************************* #

    # *********************************************************************** #
    # Lets check if any IP is listed (under listOfIPtoSearch) to be searched? #
    # If not, we'll ask user to input any specific IP they interested in      #
    # NOTE: Currently we only support to search just one IP Address.          #
    # *********************************************************************** #
    if __name__ == '__main__':
        if len(listOfIPtoSearch) == 0:
            inputOfIPtoSearch = raw_input('\nPlease enter any special IP address to search here, \n(Please enter only one IP address or hit enter):')
            if inputOfIPtoSearch == '':
                inputOfIPtoSearch = None
            else:
                inputOfIPtoSearch = inputOfIPtoSearch.strip()
                inputOfIPtoSearch = inputOfIPtoSearch.replace(" ", "")
                listOfIPtoSearch = inputOfIPtoSearch.split(",")
        
        if searchHostName == False:
            yesList =["yes", "YES", "Y", "y"]
            inputOfHostNametoSearch = raw_input('\nWould you like to display HostName info?, \n(via Reverse DNS lookup. This may add upto 10-20 minutes):')
            if inputOfHostNametoSearch not in yesList:
                searchHostName = False
            else:
                searchHostName = True
                print "[vat-log] Changing searchHostName flag to:",searchHostName
            
    # *************************************************** #
    # Lets check if the entered IP is a valid IP address? #
    # *************************************************** #
    if len(listOfIPtoSearch) > 0:
        if listOfIPtoSearch[0] != "":
            for eachIP in listOfIPtoSearch:
                print "[vat-log] IP address to search is: ", eachIP
                if validateIPs(eachIP) == False:
                    sys.exit("Error! Invalid IP address entered.")
    else:
        print "[vat-log] No IP address is provided to search"

    # *---------------------------------------------* #
    # *********************************************** #
    # Following code is the main engine of our script #
    # here we'll parse the log data,                  #
    # and save it in appropriate dictionary structure #
    # *********************************************** #
    # *---------------------------------------------* #

    for lineIndex, line in enumerate(logData):            #enumerate will allow to work on one log line at a time.
        sourceIP, sourcePort, destinationIPandPort, destinationIP, destinationPort, protocolType, trafficDirection = (None,)*7
        # In this log file we are only interested in 3 type of traffic logs. UDP, TCP, and PROTO 1 or ICMP.
        # If log line contain anyone of these three entries we'll procide, otherwise skip that line. 
        try:
            protocolType = re.search(r'UDP|TCP|PROTO 1|ICMP', line)
            if protocolType is not None:
                # Split log line to exrtract source IP here --
                split_line = line.rsplit('->',1)
                sourceIP = split_line[0].split(' ')[-1]

                # Check if source IP has source port attached, if yes extract it as source IP or keep it Null
                if len(re.findall(r'/', sourceIP)) >0:
                    sourceIP = split_line[0].split(' ')[-1].split('/')[0]
                    sourcePort = split_line[0].split(' ')[-1].split('/')[-1]
                
                destinationIPandPort = split_line[1].split(' ')[0].strip()
                destinationIP = destinationIPandPort
                # Split the destination port from destination IP
                if len(re.findall(r'/', destinationIP)) >0:
                    destinationIP = split_line[1].split(' ')[0].split('/')[0]
                    destinationPort = int(split_line[1].split(' ')[0].split('/')[-1])

                # Getting DFW Rule ID here
                if "domain" in line:
                    try:
                        dfw_ruleid_line = line[line.find("domain-"):].split(' ')
                        dfw_ruleid = int(dfw_ruleid_line[0].rsplit('/')[-1])
                        #print "dfw rule ID ", dfw_ruleid
                        if dfw_ruleid in dicOfDFWRuleid.keys():
                            dicOfDFWRuleid[dfw_ruleid] = dicOfDFWRuleid[dfw_ruleid]+1
                        else:
                            dicOfDFWRuleid[dfw_ruleid] = 1
                    except Exception as e:
                        print(e)

                # Count number of UDP, TCP, and PROTO 1 or ICMP traffic
                if protocolType.group() == "UDP":
                    udpProtoCount = udpProtoCount+1
                elif protocolType.group() == "TCP":
                    # IF traffic type is TCP and does not have a SEW or S flag we skip that log entry.
                    # - Under TCP traffic, we are only onterested in handshake traffic. 
                    if line[-4:-1] == 'SEW' or line[-2:-1] == 'S':
                        tcpProtoCount = tcpProtoCount+1
                    else:
                        continue
                elif protocolType.group() == "PROTO 1" or "ICMP":
                    proto1ProtoCount = proto1ProtoCount+1

                # Count number of In and Out traffic
                trafficDirection = re.search(r' IN | OUT ', line)
                # Check if call in In or Out, Create dic respectivally 
                if trafficDirection.group() == " IN ":
                    inTrafficCount = inTrafficCount+1

                    # Once we got the destination port - add it to dicOfINPorts - if port already exists add its count
                    #### ---- {PortNumber : [{DestinationIP : [SourseIP, ProtocolType, Count] }] ---- ####
                    if destinationPort not in dicOfINPorts.keys():
                        dicOfINPorts[destinationPort] = [{destinationIP:[sourceIP,protocolType.group(),dfw_ruleid,1]}]
                        temp_dicOfINPorts[destinationPort] = 1
                    else:
                        uniqueINPortValue = True
                        for destinationPortMetaDeta in dicOfINPorts[destinationPort]:
                            for keyOfDestinationIPOnlyMetaDeta, valueOfDestinationIPOnlyMetaDeta in destinationPortMetaDeta.items():
                                if keyOfDestinationIPOnlyMetaDeta == destinationIP and sourceIP == valueOfDestinationIPOnlyMetaDeta[0] and protocolType.group() == valueOfDestinationIPOnlyMetaDeta[1] and dfw_ruleid == valueOfDestinationIPOnlyMetaDeta[2]:
                                    valueOfDestinationIPOnlyMetaDeta[3] = valueOfDestinationIPOnlyMetaDeta[3]+1
                                    uniqueINPortValue = False
                        if uniqueINPortValue == True:
                            dicOfINPorts[destinationPort].append({destinationIP:[sourceIP,protocolType.group(),dfw_ruleid,1]})
                        temp_dicOfINPorts[destinationPort] = temp_dicOfINPorts[destinationPort] + 1

                    # Counting different type of traffic within IN direction
                    if protocolType.group() == "UDP":
                        inUDPProtoCount = inUDPProtoCount+1
                    elif protocolType.group() == "TCP":
                        inTCPProtoCount = inTCPProtoCount+1
                    elif protocolType.group() == "PROTO 1" or "ICMP":
                        inProto1ProtoCount = inProto1ProtoCount+1

                    if dfw_ruleid in dicOfINDFWRuleid.keys():
                        dicOfINDFWRuleid[dfw_ruleid] = dicOfINDFWRuleid[dfw_ruleid]+1
                    else:
                        dicOfINDFWRuleid[dfw_ruleid] = 1

                elif trafficDirection.group() == " OUT ":
                    outTrafficCount = outTrafficCount+1

                    # Once we got the destination port - add it to dicOfOUTPorts - if port already exists add its count
                    #### ---- {PortNumber : [{DestinationIP : [SourseIP, ProtocolType, Count] }] ---- ####
                    if destinationPort not in dicOfOUTPorts.keys():
                        dicOfOUTPorts[destinationPort] = [{destinationIP:[sourceIP,protocolType.group(),dfw_ruleid,1]}]
                        temp_dicOfOUTPorts[destinationPort] = 1
                    else:
                        uniqueOUTPortValue = True
                        for destinationPortMetaDeta in dicOfOUTPorts[destinationPort]:
                            for keyOfDestinationIPOnlyMetaDeta, valueOfDestinationIPOnlyMetaDeta in destinationPortMetaDeta.items():
                                if keyOfDestinationIPOnlyMetaDeta == destinationIP and sourceIP == valueOfDestinationIPOnlyMetaDeta[0] and protocolType.group() == valueOfDestinationIPOnlyMetaDeta[1] and dfw_ruleid == valueOfDestinationIPOnlyMetaDeta[2]:
                                    valueOfDestinationIPOnlyMetaDeta[3] = valueOfDestinationIPOnlyMetaDeta[3]+1
                                    uniqueOUTPortValue = False
                        if uniqueOUTPortValue == True:
                            dicOfOUTPorts[destinationPort].append({destinationIP:[sourceIP,protocolType.group(),dfw_ruleid,1]})
                        temp_dicOfOUTPorts[destinationPort] = temp_dicOfOUTPorts[destinationPort] + 1
                    # Counting different type of traffic within IN direction
                    if protocolType.group() == "UDP":
                        outUDPProtoCount = outUDPProtoCount+1
                    elif protocolType.group() == "TCP":
                        outTCPProtoCount = outTCPProtoCount+1
                    elif protocolType.group() == "PROTO 1" or "ICMP":
                        outProto1ProtoCount = outProto1ProtoCount+1

                    if dfw_ruleid in dicOfOUTDFWRuleid.keys():
                        dicOfOUTDFWRuleid[dfw_ruleid] = dicOfOUTDFWRuleid[dfw_ruleid]+1
                    else:
                        dicOfOUTDFWRuleid[dfw_ruleid] = 1

                # Add Source IP to dicOfIPs ---- ---- ---- ----
                if sourceIP not in dicOfIPs.keys():
                    dicOfIPs[sourceIP] = [{destinationIPandPort:[protocolType.group(),trafficDirection.group(),dfw_ruleid,1]}]
                else:
                    uniqueValue = True
                    for sourceIPMetaDeta in dicOfIPs[sourceIP]:
                        ##print "sourceIPMetaDeta are:", sourceIPMetaDeta
                        ##print "sourceIPMetaDeta.items() are:", sourceIPMetaDeta.items() 
                        for keyOfDestinationIPMetaDeta, valueOfDestinationIPMetaDeta in sourceIPMetaDeta.items():
                            if keyOfDestinationIPMetaDeta == destinationIPandPort and protocolType.group() == valueOfDestinationIPMetaDeta[0] and trafficDirection.group() == valueOfDestinationIPMetaDeta[1] and dfw_ruleid == valueOfDestinationIPMetaDeta[2]:
                                valueOfDestinationIPMetaDeta[3] = valueOfDestinationIPMetaDeta[3]+1
                                uniqueValue = False
                    
                    if uniqueValue == True:
                        dicOfIPs[sourceIP].append({destinationIPandPort:[protocolType.group(),trafficDirection.group(),dfw_ruleid,1]})

                # Add Destination IP to dicOfDestinationIP ---- ---- ---- ----
                if destinationIP not in dicOfDestinationIP.keys():
                    dicOfDestinationIP[destinationIP] = [{sourceIP:[protocolType.group(),trafficDirection.group(),destinationPort,dfw_ruleid,1]}]
                else:
                    uniqueDestinationIPValue = True
                    for destinationIPMetaDeta in dicOfDestinationIP[destinationIP]:
                        for keyOfSourceIPMetaDeta, valueOfSourceIPMetaDeta in destinationIPMetaDeta.items():
                            if keyOfSourceIPMetaDeta == sourceIP and protocolType.group() == valueOfSourceIPMetaDeta[0] and trafficDirection.group() == valueOfSourceIPMetaDeta[1] and destinationPort == valueOfSourceIPMetaDeta[2] and dfw_ruleid == valueOfSourceIPMetaDeta[3]:
                                valueOfSourceIPMetaDeta[4] = valueOfSourceIPMetaDeta[4]+1
                                uniqueDestinationIPValue = False
                    
                    if uniqueDestinationIPValue == True:
                        dicOfDestinationIP[destinationIP].append({sourceIP:[protocolType.group(),trafficDirection.group(),destinationPort,dfw_ruleid,1]})

            else:
                print "No UDP|TCP|PROTO 1| ICMP found in this log, line # {} - skipping to next".format(logData.index(line))
                #print "Line is: ", line
                pass
        except:
            print "[vat-log] Can't parse this log line - continuing", sys.exc_info()[0]
    ####print "dicOfDFWRuleid is: ", dicOfDFWRuleid
    ####print "dicOf IN DFWRuleid is: ", dicOfINDFWRuleid
    ####print "dicOf OUT DFWRuleid is: ", dicOfOUTDFWRuleid
    # ----------------------------------------------------
    # ----------------------------------------------------
    # ----------------------------------------------------
    ######################################################
    # ---- Working on Excel Sheet from here onwards ----
    ######################################################
    # ----------------------------------------------------
    # ----------------------------------------------------
    # ----------------------------------------------------
    print "[vat-log] Creating Excel file."
    # Creating .xlsx file
    workbook = xlsxwriter.Workbook(outputExcelFile)
    # Creating WorkSheets
    worksheet1 = workbook.add_worksheet("DFW_Summery_Page")
    worksheet2 = workbook.add_worksheet("DFW_ListAll_Source_IP")
    worksheet3 = workbook.add_worksheet("DFW_ListAll_Destination_IP")
    if len(listOfIPtoSearch)>0:
        worksheet4 = workbook.add_worksheet(listOfIPtoSearch[0])
    # Text Format variables
    wb_bold = workbook.add_format({'bold': 1})
    wb_underline = workbook.add_format({'underline': 1})
    wb_bold_underline = workbook.add_format({'underline': 1, 'bold': 1})
    wb_border_top = workbook.add_format({'top': 1})
    wb_border_left = workbook.add_format({'left': 1})
    wb_box_options = ({'width': 256,'height': 100, 'x_offset': 10, 'y_offset': 10,
        'fill': {'none': True},
        'line': {'dash_type': 'dash_dot'},
        'font': {'color': 'red','size': 14},
        'align': {'vertical': 'middle',
                  'horizontal': 'center'},})

    #Setting Column width
    worksheet1.set_column('A:A', 25)
    worksheet1.set_column('B:B', 20)
    worksheet1.set_column('E:E', 20)
    worksheet1.set_column('K:K', 20)
    worksheet1.set_column('L:L', 20)

    worksheet2.set_column('B:B', 13)
    worksheet2.set_column('C:C', 16)
    worksheet2.set_column('D:D', 16)
    worksheet2.set_column('E:E', 14)
    worksheet2.set_column('F:F', 16)
    worksheet2.set_column('G:G', 22)
    worksheet2.set_column('H:H', 13)
    worksheet2.set_column('I:I', 22)

    worksheet3.set_column('B:B', 13)
    worksheet3.set_column('C:C', 16)
    worksheet3.set_column('D:D', 16)
    worksheet3.set_column('E:E', 14)
    worksheet3.set_column('F:F', 16)
    worksheet3.set_column('G:G', 22)
    worksheet3.set_column('H:H', 13)
    worksheet3.set_column('I:I', 22)


    # Writting labels on WorkSheet2 - DFW_ListAll_Source_IP
    worksheet2.write(0, 1, "Source IP", wb_bold)
    worksheet2.write(0, 2, "Destination IP", wb_bold)
    worksheet2.write(0, 3, "Destination Port", wb_bold)
    worksheet2.write(0, 4, "Protocol Type", wb_bold)
    worksheet2.write(0, 5, "Traffic Direction", wb_bold)
    worksheet2.write(0, 6, "Number of Connections", wb_bold)
    worksheet2.write(0, 7, "DFW Rule ID", wb_bold)
    worksheet2.write(0, 8, "Destination Host", wb_bold)

    # Writting labels on WorkSheet3 - DFW_ListAll_Destination_IP
    worksheet3.write(0, 1, "Destination IP", wb_bold)
    worksheet3.write(0, 2, "Destination Port", wb_bold)
    worksheet3.write(0, 3, "Source IP", wb_bold)
    worksheet3.write(0, 4, "Protocol Type", wb_bold)
    worksheet3.write(0, 5, "Traffic Direction", wb_bold)
    worksheet3.write(0, 6, "Number of Connections", wb_bold)
    worksheet3.write(0, 7, "DFW Rule ID", wb_bold)
    worksheet3.write(0, 8, "Source Host", wb_bold)

    # ----------------------------------------------------
    #######################################################################
    # ---- Writting all the data to first Worksheet - DFW_Summery_Page ----
    #######################################################################
    # ----------------------------------------------------
    print "[vat-log] Creating First Worksheet..."

    worksheet1.insert_image('B2', 'vmware-logo.png')

    welcome_Text = 'Thank you for using DFW VATool'
    options = ({'align': {'vertical': 'middle','horizontal': 'center'},
        'x_offset': 10,'y_offset': 10,
        'gradient': {'colors': ['#DDEBCF',
                                '#9CB86E',
                                '#156B13']},
        'border': {'color': 'green','width': 3,
                   'dash_type': 'round_dot'},
        'width': 418,
        'height': 220,
        'font': {'bold': True,
                 #'italic': True,
                 #'underline': True,
                 'name': 'Arial',
                 'color': 'black',
                 'size': 13},})
                 #worksheet1.insert_textbox(11, 1, welcome_Text, options)
    pathToTempDir= os.path.join(current_Dir, 'temp/')
    listOfLogFiles = os.listdir(pathToTempDir)
    strOfLogFies = ""
    for the_file in listOfLogFiles:
        if "dfwpktlogsVAT" in the_file:
            listOfLogFiles.remove(the_file)
        else:
            strOfLogFies = str(strOfLogFies + the_file + " ")

    summery_Text = ("Name of input log file(s) used: "+ strOfLogFies + " \nName of output excel file: dfwpktlogsVAT.xlsx" + 
        "\n\nNumber of lines parsed in this log file: "+ str(numberOfLogs) + "\n\nLog file start timestamp: " + startLogDate + "\nLog file end timestamp: " + endLogDate)
    #worksheet1.insert_textbox(19, 1, summery_Text, {'width': 301,'x_offset': 10,'y_offset': 10,})
    worksheet1.insert_textbox(11, 1, summery_Text, options)

    # Defining Lables for Pie graph of IN vs OUT Traffic - The slices will be ordered and plotted counter-clockwise.
    worksheet1.write(25, 0, "Distribution of IN vs OUT traffic", wb_bold)

    worksheet1.write(26, 0, "Total IN traffic:")
    worksheet1.write(27, 0, "Total OUT traffic:")
    worksheet1.write(26, 1, inTrafficCount)
    worksheet1.write(27, 1, outTrafficCount)

    inVsOutSizesChart = workbook.add_chart({'type': 'pie'})

    inVsOutSizesChart.add_series({
        'categories': '=DFW_Summery_Page!$A$27:$A$28',
        'values':     '=DFW_Summery_Page!$B$27:$B$28',
        'data_labels': {'percentage': True},
        'points': [
            {'fill': {'color': 'green'}},
            {'fill': {'color': 'red'}},
        ],
    })

    # Set Title of the pie chart
    inVsOutSizesChart.set_title({'name': 'TRAFFIC: IN vs OUT'})
    # Set size of the pie chart
    inVsOutSizesChart.set_size({'x_scale': 0.8, 'y_scale': 1})
    # Set an Excel chart style. Colors with white outline and shadow.
    inVsOutSizesChart.set_style(10)
    # Insert the chart into the worksheet.
    worksheet1.insert_chart('A30', inVsOutSizesChart, {'x_offset': 25, 'y_offset': 10})


    #######################################################################
    # Creating Three Pie Charts to show Traffic type (UDP, TCP, Proto 1, ICMP) 
    # for  All traffic, Only IN Traffic, and Only OUT Traffic.
    #######################################################################

    # Defining Lables for Pie graph of % trappic type UDP vs TCP vs PROTO 1
    trafficTypeLabels = 'UDP Traffic', 'TCP Traffic', 'ICMP Traffic'
    trafficTypeColors = ['lightskyblue', 'lightcoral', 'yellowgreen'] ##colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
    trafficTypeExplode = (0, 0, 0)  # only "explode" the 2nd slice 
    trafficTypeSizes = [udpProtoCount, tcpProtoCount, proto1ProtoCount]

    worksheet1.write(45, 0, "-------------------------------")
    worksheet1.write(46, 0, "Distribution of Traffic Type", wb_bold)

    worksheet1.write(47, 0, "Total UDP traffic:")
    worksheet1.write(47, 1, udpProtoCount)

    worksheet1.write(48, 0, "Total TCP traffic:")
    worksheet1.write(48, 1, tcpProtoCount)

    worksheet1.write(49, 0, "Total ICMP traffic:")
    worksheet1.write(49, 1, proto1ProtoCount)

    ########################################################
    # Creating Pie Chart to show the Ports of ALL traffic.
    #######################################################
    #Traffic Chart #1 - Traffic type for all traffic

    trafficTypeSizesChart = workbook.add_chart({'type': 'pie'})

    trafficTypeSizesChart.add_series({
        'categories': '=DFW_Summery_Page!$A$48:$A$50',
        'values':     '=DFW_Summery_Page!$B$48:$B$50',
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    trafficTypeSizesChart.set_title({'name': 'UDP vs TCP vs ICMP - All'})
    # Set size of the pie chart
    trafficTypeSizesChart.set_size({'x_scale': 0.8, 'y_scale': 1})
    # Set an Excel chart style. Colors with white outline and shadow.
    trafficTypeSizesChart.set_style(10)
    # Insert the chart into the worksheet
    worksheet1.insert_chart('A52', trafficTypeSizesChart, {'x_offset': 25, 'y_offset': 10})

    #Traffic chart #2 - Traffic type for IN traffic
    worksheet1.write(47, 4, "Total UDP IN traffic:")
    worksheet1.write(47, 5, inUDPProtoCount)

    worksheet1.write(48, 4, "Total TCP IN traffic:")
    worksheet1.write(48, 5, inTCPProtoCount)

    worksheet1.write(49, 4, "Total ICMP IN traffic:")
    worksheet1.write(49, 5, inProto1ProtoCount)

    inTrafficTypeSizesChart = workbook.add_chart({'type': 'pie'})

    inTrafficTypeSizesChart.add_series({
        'categories': '=DFW_Summery_Page!$E$48:$E$50',
        'values':     '=DFW_Summery_Page!$F$48:$F$50',
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    inTrafficTypeSizesChart.set_title({'name': 'UDP vs TCP vs ICMP - IN Traffic'})
    # Set size of the pie chart
    inTrafficTypeSizesChart.set_size({'x_scale': 0.9, 'y_scale': 1})
    # Set an Excel chart style. Colors with white outline and shadow.
    inTrafficTypeSizesChart.set_style(10)
    # Insert the chart into the worksheet
    worksheet1.insert_chart('E52', inTrafficTypeSizesChart, {'x_offset': 25, 'y_offset': 10})


    #Traffic chart #3 - Traffic type for OUT traffic
    worksheet1.write(47, 10, "Total UDP OUT traffic:")
    worksheet1.write(47, 11, outUDPProtoCount)

    worksheet1.write(48, 10, "Total TCP OUT traffic:")
    worksheet1.write(48, 11, outTCPProtoCount)

    worksheet1.write(49, 10, "Total ICMP OUT traffic")
    worksheet1.write(49, 11, outProto1ProtoCount)

    outTrafficTypeSizesChart = workbook.add_chart({'type': 'pie'})

    outTrafficTypeSizesChart.add_series({
        'categories': '=DFW_Summery_Page!$K$48:$K$50',
        'values':     '=DFW_Summery_Page!$L$48:$L$50',
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    outTrafficTypeSizesChart.set_title({'name': 'UDP vs TCP vs ICMP - OUT Traffic'})
    # Set size of the pie chart
    outTrafficTypeSizesChart.set_size({'x_scale': 0.9, 'y_scale': 1})
    # Set an Excel chart style. Colors with white outline and shadow.
    outTrafficTypeSizesChart.set_style(10)
    # Insert the chart into the worksheet
    worksheet1.insert_chart('K52', outTrafficTypeSizesChart, {'x_offset': 25, 'y_offset': 10})

    worksheet1.write(67, 0, "-------------------------------")

    #######################################################################
    # Creating Pie Chart to show the Ports of IN traffic.
    #######################################################################
    # First PORTs Graph
    worksheet1.write(69, 0, "Distribution of IN Traffic Ports", wb_bold)
    worksheet1.write(70, 0, "Total IN Ports:")
    worksheet1.write(70, 1, len(temp_dicOfINPorts))
    worksheet1.write(71, 0, "Port Number", wb_underline)
    worksheet1.write(71, 1, "Number of Log Entries", wb_underline)

    #print "temp_dicOfINPorts is:", temp_dicOfINPorts
    ##for dynamicExcelRowIndex, eachINPort, eachINPortCount in enumerate(temp_dicOfINPorts.items()):
    dynamicExcelRowIndex=72
    sorted_temp_dicOfINPorts = sorted(temp_dicOfINPorts.items(), key=operator.itemgetter(1))
    ##for eachINPort, eachINPortCount in temp_dicOfINPorts.items():
    for eachINPort, eachINPortCount in sorted_temp_dicOfINPorts[::-1]:
        try:
            if eachINPort in portNumAssociationDic.keys():
                eachINPort = str(eachINPort)+"-"+portNumAssociationDic[eachINPort]
        except Exception, e:
            print "Skipping Error at adding name to port number", e

        dynamicExcelRowIndex=dynamicExcelRowIndex+1
        worksheet1.write(dynamicExcelRowIndex, 0, eachINPort)
        worksheet1.write(dynamicExcelRowIndex, 1, eachINPortCount)
        if (100 * float(int(eachINPortCount)/float(int(inTrafficCount)))) > 5:
            numberOfINPortsForGraph +=1
            listOfTopINPorts.append(eachINPort)
        else:
            totalINMisCount = totalINMisCount + eachINPortCount

        #print "Index is: ", dynamicExcelRowIndex
        #print "IN Port is: ", eachINPort
        #print "IN Port count is: ", eachINPortCount

    dynamicExcelRowIndex += 1
    bottomCellForINPortList = dynamicExcelRowIndex
    bottomCellForGraphValueIN = 72 + numberOfINPortsForGraph
    inTrafficPortsChart = workbook.add_chart({'type': 'pie'})

    worksheet1.write(72, 0, "All Ports under 5%")
    worksheet1.write(72, 1, totalINMisCount)

    inTrafficPortsChart.add_series({
        'categories': '=DFW_Summery_Page!$A$73:$A$'+str(bottomCellForGraphValueIN),
        'values':     '=DFW_Summery_Page!$B$73:$B$'+str(bottomCellForGraphValueIN),
        'data_labels': {'percentage': True},
    })

    # Set Title of the pie chart
    inTrafficPortsChart.set_title({'name': 'IN Traffic Port Distribution'})
    # Set size of the pie chart
    inTrafficPortsChart.set_size({'x_scale': 1, 'y_scale': 1.5})
    # Set an Excel chart style. Colors with white outline and shadow.
    inTrafficPortsChart.set_style(10)
    # Insert the chart into the worksheet
    ##worksheet2.insert_chart('A'+str(dynamicExcelRowIndex), inTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})
    worksheet1.insert_chart('C72', inTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})

    #######################################################################
    # Creating Pie Chart to show the Ports of OUT traffic.
    #######################################################################
    # Second PORTs Graph
    worksheet1.write(69, 10, "Distribution of OUT Traffic Ports", wb_bold)
    worksheet1.write(70, 10, "Total OUT Ports:")
    worksheet1.write(70, 11, len(temp_dicOfOUTPorts))
    worksheet1.write(71, 10, "Port Number", wb_underline)
    worksheet1.write(71, 11, "Number of Log Entries", wb_underline)

    dynamicExcelRowIndex=72
    sorted_temp_dicOfOUTPorts = sorted(temp_dicOfOUTPorts.items(), key=operator.itemgetter(1))
    for eachOUTPort, eachOUTPortCount in sorted_temp_dicOfOUTPorts[::-1]:
        try:
            '''
            if eachOUTPort!= None:
                eachOUTPort = eachOUTPort.rstrip()
            else:
                eachOUTPort = 'None'
            '''
            if eachOUTPort in portNumAssociationDic.keys():
                eachOUTPort = str(eachOUTPort)+"-"+portNumAssociationDic[eachOUTPort]
        except Exception, e:
            print "Skipping Error at adding name to port number", e
        dynamicExcelRowIndex=dynamicExcelRowIndex+1
        worksheet1.write(dynamicExcelRowIndex, 10, eachOUTPort)
        worksheet1.write(dynamicExcelRowIndex, 11, eachOUTPortCount)

        if (100 * float(int(eachOUTPortCount)/float(int(outTrafficCount)))) > 5:
            numberOfOUTPortsForGraph +=1
            listOfTopOUTPorts.append(eachOUTPort)
        else:
            totalOUTMisCount = totalOUTMisCount + eachOUTPortCount

    dynamicExcelRowIndex += 1
    bottomCellForOUTPortList = dynamicExcelRowIndex
    bottomCellForGraphValueOUT = 72 + numberOfOUTPortsForGraph
    outTrafficPortsChart = workbook.add_chart({'type': 'pie'})

    worksheet1.write(72, 10, "All Ports under 5%")
    worksheet1.write(72, 11, totalOUTMisCount)

    outTrafficPortsChart.add_series({
        'categories': '=DFW_Summery_Page!$K$73:$K$'+str(bottomCellForGraphValueOUT),
        'values':     '=DFW_Summery_Page!$L$73:$L$'+str(bottomCellForGraphValueOUT),
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    outTrafficPortsChart.set_title({'name': 'OUT Traffic Port Distribution'})
    # Set size of the pie chart
    outTrafficPortsChart.set_size({'x_scale': 1, 'y_scale': 1.5})
    # Set an Excel chart style. Colors with white outline and shadow.
    outTrafficPortsChart.set_style(10)
    # Insert the chart into the worksheet
    ##worksheet2.insert_chart('A'+str(dynamicExcelRowIndex), inTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})
    worksheet1.insert_chart('M72', outTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})

    ###################################################
    # Creating Pie Chart to show the use of DFW Rules.
    ###################################################

    
    if int(bottomCellForINPortList) > int(bottomCellForOUTPortList):
        dfw_ruleid_start_cell = int(bottomCellForINPortList)+1
    else:
        dfw_ruleid_start_cell = int(bottomCellForOUTPortList)+1

    if dfw_ruleid_start_cell < 94:
        dfw_ruleid_start_cell = 95

    worksheet1.write(dfw_ruleid_start_cell, 0, "-------------------------------")
    worksheet1.write(dfw_ruleid_start_cell+1, 0, "Distribution of DFW Rules", wb_bold)

    worksheet1.write(dfw_ruleid_start_cell+2, 0, "Total dfw rules used for IN traffic:")
    worksheet1.write(dfw_ruleid_start_cell+2, 1, len(dicOfINDFWRuleid))
    worksheet1.write(dfw_ruleid_start_cell+3, 0, "DFW Rule ID", wb_underline)
    worksheet1.write(dfw_ruleid_start_cell+3, 1, "Number of Log Entries", wb_underline)

    worksheet1.write(dfw_ruleid_start_cell+2, 10, "Total dfw rules used for OUT traffic:")
    worksheet1.write(dfw_ruleid_start_cell+2, 11, len(dicOfOUTDFWRuleid))
    worksheet1.write(dfw_ruleid_start_cell+3, 10, "DFW Rule ID", wb_underline)
    worksheet1.write(dfw_ruleid_start_cell+3, 11, "Number of Log Entries", wb_underline)
    dfw_ruleid_IN_start_cell = dfw_ruleid_start_cell+4
    dfw_ruleid_OUT_start_cell = dfw_ruleid_start_cell+4
    
    sorted_dicOfINDFWRuleid = sorted(dicOfINDFWRuleid.items(), key=operator.itemgetter(0))
    sorted_dicOfOUTDFWRuleid = sorted(dicOfOUTDFWRuleid.items(), key=operator.itemgetter(0))

    for myINRuleID in sorted_dicOfINDFWRuleid:
        try:
            worksheet1.write(dfw_ruleid_IN_start_cell, 0, myINRuleID[0])
            worksheet1.write(dfw_ruleid_IN_start_cell, 1, myINRuleID[1])
            dfw_ruleid_IN_start_cell +=1
        except Exception, e:
            print "Skipping Error: When adding IN DFW Rule ID to Excel", e

    inTrafficRuleIDChart = workbook.add_chart({'type': 'pie'})

    inTrafficRuleIDChart.add_series({
        'categories': '=DFW_Summery_Page!$A$'+str(dfw_ruleid_start_cell+5)+':$A$'+str(dfw_ruleid_IN_start_cell),
        'values':     '=DFW_Summery_Page!$B$'+str(dfw_ruleid_start_cell+5)+':$B$'+str(dfw_ruleid_IN_start_cell),
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    inTrafficRuleIDChart.set_title({'name': 'DFW Rules Distribution - IN Traffic'})
    # Set size of the pie chart
    inTrafficRuleIDChart.set_size({'x_scale': 1, 'y_scale': 1.5})
    # Set an Excel chart style. Colors with white outline and shadow.
    inTrafficRuleIDChart.set_style(10)
    # Insert the chart into the worksheet
    ##worksheet2.insert_chart('A'+str(dynamicExcelRowIndex), inTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})
    worksheet1.insert_chart('C'+str(dfw_ruleid_start_cell+3), inTrafficRuleIDChart, {'x_offset': 25, 'y_offset': 10})


    for myOUTRuleID in sorted_dicOfOUTDFWRuleid:
        try:
            worksheet1.write(dfw_ruleid_OUT_start_cell, 10, myOUTRuleID[0])
            worksheet1.write(dfw_ruleid_OUT_start_cell, 11, myOUTRuleID[1])
            dfw_ruleid_OUT_start_cell +=1
        except Exception, e:
            print "Skipping Error: When adding OUT DFW Rule ID to Excel", e

    outTrafficRuleIDChart = workbook.add_chart({'type': 'pie'})

    outTrafficRuleIDChart.add_series({
        'categories': '=DFW_Summery_Page!$K$'+str(dfw_ruleid_start_cell+5)+':$K$'+str(dfw_ruleid_OUT_start_cell),
        'values':     '=DFW_Summery_Page!$L$'+str(dfw_ruleid_start_cell+5)+':$L$'+str(dfw_ruleid_OUT_start_cell),
        'data_labels': {'percentage': True},
    })
    # Set Title of the pie chart
    outTrafficRuleIDChart.set_title({'name': 'DFW Rules Distribution - OUT Traffic'})
    # Set size of the pie chart
    outTrafficRuleIDChart.set_size({'x_scale': 1, 'y_scale': 1.5})
    # Set an Excel chart style. Colors with white outline and shadow.
    outTrafficRuleIDChart.set_style(10)
    # Insert the chart into the worksheet
    ##worksheet2.insert_chart('A'+str(dynamicExcelRowIndex), inTrafficPortsChart, {'x_offset': 25, 'y_offset': 10})
    worksheet1.insert_chart('M'+str(dfw_ruleid_start_cell+3), outTrafficRuleIDChart, {'x_offset': 25, 'y_offset': 10})

    # ----------------------------------------------------
    #############################################################################
    # ---- Writting all the data to second Worksheet - DFW_ListAll_Source_IP ----
    #############################################################################
    # ----------------------------------------------------
    print "[vat-log] Creating Second Worksheet..."
    rowCount = 0
    tempCount = 0
    totalIPSearchedForHostName = 0

    for keySourceIP, valueSourceIP in dicOfIPs.items():
        rowCount += 1
        tempCount += 1
        worksheet2.write(rowCount, 0, tempCount)
        worksheet2.write(rowCount, 1, keySourceIP)

        if len(dicOfIPs[keySourceIP]) > 4:
            rowCount += 1
            tableStartPosition = "C"+str(rowCount)
            tableEndPosition = "I" + str(int(rowCount)+len(dicOfIPs[keySourceIP]))
            worksheet2.add_table(tableStartPosition+':'+tableEndPosition, 
                {'columns': [{'header': 'Destination IP'},{'header': 'Destination Port'},{'header': 'Protocol Type'},
                {'header': 'Traffic Direction'},{'header': 'Number of Connections'},{'header': 'DFW RuleID'},{'header': 'Destination Host'},],
                'style': 'Table Style Light 11'})

        for traffic in valueSourceIP:
            for myTrafficKey, myTrafficValue in traffic.items():
                if len(re.findall(r'/', myTrafficKey)) >0:
                    trafficDestinationIP = myTrafficKey.split('/')[0]
                    trafficDestinationPort = int(myTrafficKey.split('/')[-1])
                else:
                    trafficDestinationIP = myTrafficKey
                    trafficDestinationPort = None
                # Checking if User selected to search for host name. If yes, search host name by calling getHostName function and 
                # adding the values to dictionary tempDicOfAllTrafficIP.
                if searchHostName == True:
                    if validateIPs(trafficDestinationIP):
                        if trafficDestinationIP in tempDicOfAllTrafficIP.keys():
                            pass
                        else:
                            totalIPSearchedForHostName +=1
                            newProcess_destinationHostName = multiprocessing.Process(target=getHostName, args=(trafficDestinationIP,tempDicOfAllTrafficIP))
                            newProcess_destinationHostName.start()
                            newProcess_destinationHostName.join(1)

                            if newProcess_destinationHostName.is_alive():
                                ##print "Its taking too long! Terminating the process of getting host name for ", trafficDestinationIP
                                newProcess_destinationHostName.terminate()
                                newProcess_destinationHostName.join()
                                tempDicOfAllTrafficIP[trafficDestinationIP] = None
                    else:
                        tempDicOfAllTrafficIP[trafficDestinationIP] = None

                worksheet2.write(rowCount, 2, trafficDestinationIP)
                worksheet2.write(rowCount, 3, trafficDestinationPort)
                worksheet2.write(rowCount, 4, myTrafficValue[0])
                worksheet2.write(rowCount, 5, myTrafficValue[1])
                worksheet2.write(rowCount, 6, myTrafficValue[3])
                worksheet2.write(rowCount, 7, myTrafficValue[2])

                if trafficDestinationIP in tempDicOfAllTrafficIP.keys():
                    if tempDicOfAllTrafficIP[trafficDestinationIP] != None:
                        worksheet2.write(rowCount, 8, tempDicOfAllTrafficIP[trafficDestinationIP])
            rowCount += 1
            #print rowCount

    # ----------------------------------------------------
    #################################################################################
    # ---- Writting all the data to third Worksheet - DFW_ListAll_Destination_IP ----
    #################################################################################
    # ----------------------------------------------------
    print "[vat-log] Creating Third Worksheet..."
    rowCount = 0
    tempCount = 0

    for keyDestinationIP, valueDestinationIP in dicOfDestinationIP.items():
        rowCount += 1
        tempCount += 1
        worksheet3.write(rowCount, 0, tempCount)
        worksheet3.write(rowCount, 1, keyDestinationIP)
        #print "Number of entries in this IP {} is {}".format(keyDestinationIP, len(dicOfDestinationIP[keyDestinationIP]))
        
        if len(dicOfDestinationIP[keyDestinationIP]) > 4:
            rowCount += 1
            tableStartPosition = "C"+str(rowCount)
            tableEndPosition = "I" + str(int(rowCount)+len(dicOfDestinationIP[keyDestinationIP]))
            worksheet3.add_table(tableStartPosition+':'+tableEndPosition, 
                {'columns': [{'header': 'Destination Port'},{'header': 'Source IP'},{'header': 'Protocol Type'},
                {'header': 'Traffic Direction'},{'header': 'Number of Connections'},{'header': 'DFW RuleID'},{'header': 'Source Host'},],
                'style': 'Table Style Light 13'})

        for destinationTraffic in valueDestinationIP:
            for myDestinationTrafficKey, myDestinationTrafficValue in destinationTraffic.items():
                if len(re.findall(r'/', myDestinationTrafficKey)) >0:
                    trafficSourceIP = myDestinationTrafficKey.split('/')[0]
                    trafficDestinationPort = int(myDestinationTrafficKey.split('/')[-1])
                else:
                    trafficSourceIP = myDestinationTrafficKey
                    trafficDestinationPort = None

                # Checking if User selected to search for host name. If yes, search host name by calling getHostName function and 
                # adding the values to dictionary tempDicOfAllTrafficIP.
                if searchHostName == True:
                    if validateIPs(trafficSourceIP):
                        if trafficSourceIP in tempDicOfAllTrafficIP.keys():
                            pass
                        else:
                            totalIPSearchedForHostName +=1
                            newProcess_sourceHostName = multiprocessing.Process(target=getHostName, args=(trafficSourceIP, tempDicOfAllTrafficIP))
                            newProcess_sourceHostName.start()
                            newProcess_sourceHostName.join(1)

                            if newProcess_sourceHostName.is_alive():
                                ##print "Its taking too long! Terminating the process of getting host name for ", trafficSourceIP
                                newProcess_sourceHostName.terminate()
                                newProcess_sourceHostName.join()
                                tempDicOfAllTrafficIP[trafficSourceIP] = None
                    else:
                        tempDicOfAllTrafficIP[trafficSourceIP] = None

                worksheet3.write(rowCount, 2, myDestinationTrafficValue[2])
                worksheet3.write(rowCount, 3, trafficSourceIP)
                worksheet3.write(rowCount, 4, myDestinationTrafficValue[0])
                worksheet3.write(rowCount, 5, myDestinationTrafficValue[1])
                worksheet3.write(rowCount, 6, myDestinationTrafficValue[4])
                worksheet3.write(rowCount, 7, myDestinationTrafficValue[3])
                if trafficSourceIP in tempDicOfAllTrafficIP.keys():
                    if tempDicOfAllTrafficIP[trafficSourceIP] != None:
                        worksheet3.write(rowCount, 8, tempDicOfAllTrafficIP[trafficSourceIP])
            rowCount += 1
            #print rowCount
    print "Total Host Name searched:", totalIPSearchedForHostName
    print "Total IPs on tempDicOfAllTrafficIP:", len(tempDicOfAllTrafficIP)

    # ----------------------------------------------------
    #################################################################################
    # ---- Writting all the data to dynamic fourth Worksheet - DFW_IP_Charts_Data ----
    #################################################################################
    # ----------------------------------------------------

    if len(listOfIPtoSearch)!=0:
        print "[vat-log] Creating Fourth Worksheet..."
        try:
            worksheet4.write(0, 0, "Custom Page for IP", wb_bold_underline)
            worksheet4.write(0, 1, listOfIPtoSearch[0], wb_bold_underline)
            worksheet4.set_column('A:A', 31)
            worksheet4.set_column('B:B', 13)
            worksheet4.set_column('C:C', 13)
            worksheet4.set_column('D:D', 13)
            worksheet4.set_column('F:F', 31)
            worksheet4.set_column('G:G', 13)
            worksheet4.set_column('H:H', 13)
            worksheet4.set_column('I:I', 13)
            worksheet4.set_column('K:K', 31)
            worksheet4.set_column('L:L', 13)
            worksheet4.set_column('M:M', 13)
            worksheet4.set_column('N:N', 13)
            worksheet4.set_column('P:P', 31)
            worksheet4.set_column('Q:Q', 13)
            worksheet4.set_column('R:R', 13)
            worksheet4.set_column('S:S', 13)

            worksheet4.write(21, 0, "Searched "+listOfIPtoSearch[0]+" as Source IP", wb_bold_underline)
            worksheet4.write(24, 0, "List of destination IPs w/ IN traffic", wb_bold)
            worksheet4.write(24, 1, "Port Numbers", wb_bold)
            worksheet4.write(24, 2, "Traffic Type", wb_bold)
            worksheet4.write(24, 3, "Numbers of Connections", wb_bold)
            worksheet4.write(24, 4, "DFW Rule ID", wb_bold)
            worksheet4.write(24, 5, "Destination HostName", wb_bold)

            worksheet4.write(24, 7, "List of destination IPs w/ OUT traffic", wb_bold)
            worksheet4.write(24, 8, "Port Numbers", wb_bold)
            worksheet4.write(24, 9, "Traffic Type", wb_bold)
            worksheet4.write(24, 10, "Numbers of Connections", wb_bold)
            worksheet4.write(24, 11, "DFW Rule ID", wb_bold)
            worksheet4.write(24, 12, "Destination HostName", wb_bold)

            worksheet4.write(21, 14, "Searched "+listOfIPtoSearch[0]+" as Destination IP", wb_bold_underline)
            worksheet4.write(24, 14, "List of source IPs w/ IN traffic", wb_bold)
            worksheet4.write(24, 15, "Port Numbers", wb_bold)
            worksheet4.write(24, 16, "Traffic Type", wb_bold)
            worksheet4.write(24, 17, "Numbers of Connections", wb_bold)
            worksheet4.write(24, 18, "DFW Rule ID", wb_bold)
            worksheet4.write(24, 19, "Souece HostName", wb_bold)

            worksheet4.write(24, 21, "List of source IPs w/ OUT traffic", wb_bold)
            worksheet4.write(24, 22, "Port Numbers", wb_bold)
            worksheet4.write(24, 23, "Traffic Type", wb_bold)
            worksheet4.write(24, 24, "Numbers of Connections", wb_bold)
            worksheet4.write(24, 25, "DFW Rule ID", wb_bold)
            worksheet4.write(24, 26, "Souece HostName", wb_bold)

            customIPINTrafficCount, customIPOUTTrafficCount, customIPINTrafficCompressCount, customIPOUTTrafficCompressCount  = (0,)*4

            printRowSourceINCounter = 25
            printRowSourceOUTCounter = 25

            for keySourceIP, valueSourceIP in dicOfIPs.items():
                
                listOfINIPsForSourceIP = []
                listOfOUTIPsForSourceIP = []
                '''
                print "keySourceIP is:",keySourceIP
                print "keySourceIP type is:",type(keySourceIP)
                print "keySourceIP length is:",len(keySourceIP)
                print "List Of IPs to Search",listOfIPtoSearch
                print "List Of IPs to Search type is:",type(listOfIPtoSearch[0])
                print "List Of IPs to Search length is:",len(listOfIPtoSearch[0])
                '''
                # Find searched IP in Source IPs
                if keySourceIP in listOfIPtoSearch:
                    #print "IP I'm looking for is in source IP {} and its data is {}:".format(keySourceIP,dicOfIPs[keySourceIP])
                    #print "length of dictionary is: ", len(dicOfIPs[keySourceIP])
                    for eachSourceIPMetaData in dicOfIPs[keySourceIP]:
                        tempDestinationIPandPort = eachSourceIPMetaData.keys()
                        if '/' in tempDestinationIPandPort[0]:
                            tempDestinationIP = tempDestinationIPandPort[0].split('/')[0]
                            tempDestinationPort = int(tempDestinationIPandPort[0].split('/')[1])
                        else:
                            tempDestinationIP = tempDestinationIPandPort[0]
                            tempDestinationPort = None
                        #print "eachSourceIPMetaData keys are:", eachSourceIPMetaData.keys()
                        #print "eachSourceIPMetaData values are:", eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]]
                        if eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][1] == ' IN ':
                            #print "Found OUT Traffic"
                            customIPINTrafficCount += int(eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][2])
                            worksheet4.write(printRowSourceINCounter, 0, tempDestinationIP, wb_border_left)
                            worksheet4.write(printRowSourceINCounter, 1, tempDestinationPort)
                            worksheet4.write(printRowSourceINCounter, 2, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][0])
                            worksheet4.write(printRowSourceINCounter, 3, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][3])
                            worksheet4.write(printRowSourceINCounter, 4, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][2])
                            printRowSourceINCounter+=1
                        elif eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][1] == ' OUT ':
                            customIPOUTTrafficCount += int(eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][2])
                            worksheet4.write(printRowSourceOUTCounter, 7, tempDestinationIP, wb_border_left)
                            worksheet4.write(printRowSourceOUTCounter, 8, tempDestinationPort)
                            worksheet4.write(printRowSourceOUTCounter, 9, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][0])
                            worksheet4.write(printRowSourceOUTCounter, 10, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][3])
                            worksheet4.write(printRowSourceOUTCounter, 11, eachSourceIPMetaData[eachSourceIPMetaData.keys()[0]][2])
                            printRowSourceOUTCounter+=1
                            #print "Found IN Traffic"

            if printRowSourceINCounter > 26:
                tableStartPosition = "A25"
                tableEndPosition = "F" + str(printRowSourceINCounter)
                worksheet4.add_table(tableStartPosition+':'+tableEndPosition, 
                    {'columns': [{'header': 'List of destination IPs w/ IN traffic'},{'header': 'Port Numbers'},
                    {'header': 'Traffic Type'},{'header': 'Numbers of Connections'},{'header': 'DFW Rule ID'},{'header': 'Destination HostName'},]})

            if printRowSourceOUTCounter > 26:
                tableStartPosition = "H25"
                tableEndPosition = "M" + str(printRowSourceOUTCounter)
                worksheet4.add_table(tableStartPosition+':'+tableEndPosition, 
                    {'columns': [{'header': 'List of destination IPs w/ OUT traffic'},{'header': 'Port Numbers'},
                    {'header': 'Traffic Type'},{'header': 'Numbers of Connections'},{'header': 'DFW Rule ID'},{'header': 'Destination HostName'}]})


            printRowDestinationINCounter = 25
            printRowDestinationOUTCounter = 25
            # Find searched IP in Destination IPs
            for keySourceIP, valueSourceIP in dicOfIPs.items():
                for eachOfSourceIPValue in valueSourceIP:
                    tempDestinationIPandPort = eachOfSourceIPValue.keys()
                    if '/' in tempDestinationIPandPort[0]:
                        tempDestinationIP = tempDestinationIPandPort[0].split('/')[0]
                        tempDestinationPort = int(tempDestinationIPandPort[0].split('/')[1])
                    else:
                        tempDestinationIP = tempDestinationIPandPort[0]
                        tempDestinationPort = None

                    if tempDestinationIP in listOfIPtoSearch:
                        #print "IP I'm looking for is in Destination IP {} and its data is {}:".format(keySourceIP, eachOfSourceIPValue)
                        #print "In Source IP traffic IP is: ",keySourceIP
                        #print "In Source IP traffic direction is: ",eachOfSourceIPValue.items()[0][1][1]
                        if eachOfSourceIPValue.items()[0][1][1] == ' IN ':
                            #print "Found IN traffic in Destination IP!!"
                            ##customIPINTrafficCount += int(eachOfSourceIPValue.items()[0][1][2])
                            worksheet4.write(printRowDestinationINCounter, 14, keySourceIP, wb_border_left)
                            worksheet4.write(printRowDestinationINCounter, 15, tempDestinationPort)
                            worksheet4.write(printRowDestinationINCounter, 16, eachOfSourceIPValue.items()[0][1][0])
                            worksheet4.write(printRowDestinationINCounter, 17, eachOfSourceIPValue.items()[0][1][3])
                            worksheet4.write(printRowDestinationINCounter, 18, eachOfSourceIPValue.items()[0][1][2])
                            printRowDestinationINCounter+=1
                            customIPINTrafficCompressCount+=1
                            customIPINTrafficCount += int(eachOfSourceIPValue.items()[0][1][2])

                            if tempDestinationIP not in dicOfINDestinationIP.keys():
                                dicOfINDestinationIP[tempDestinationIP] = [{keySourceIP:[tempDestinationPort,eachOfSourceIPValue.items()[0][1][0],1]}]
                                ##temp_dicOfINPorts[destinationPort] = 1
                            else:
                                uniqueINDestinationIPValue = True
                                for sourceIPMetaDeta in dicOfINDestinationIP[tempDestinationIP]:
                                    for keyOfSourceIPOnlyMetaDeta, valueOfSourceIPOnlyMetaDeta in sourceIPMetaDeta.items():
                                        if keyOfSourceIPOnlyMetaDeta == keySourceIP and tempDestinationPort == valueOfSourceIPOnlyMetaDeta[0] and eachOfSourceIPValue.items()[0][1][0] == valueOfSourceIPOnlyMetaDeta[1]:
                                            valueOfSourceIPOnlyMetaDeta[2] = valueOfSourceIPOnlyMetaDeta[2]+1
                                            uniqueINDestinationIPValue = False
                                if uniqueINDestinationIPValue == True:
                                    dicOfINDestinationIP[tempDestinationIP].append({keySourceIP:[tempDestinationPort,eachOfSourceIPValue.items()[0][1][0],1]})
                                ##temp_dicOfINPorts[tempDestinationIP] = temp_dicOfINPorts[tempDestinationIP] + 1

                        elif eachOfSourceIPValue.items()[0][1][1] == ' OUT ':
                            #print "We Found OUT traffic in Destination IP!!"
                            worksheet4.write(printRowDestinationOUTCounter, 21, keySourceIP, wb_border_left)
                            worksheet4.write(printRowDestinationOUTCounter, 22, tempDestinationPort)
                            worksheet4.write(printRowDestinationOUTCounter, 23, eachOfSourceIPValue.items()[0][1][0])
                            worksheet4.write(printRowDestinationOUTCounter, 24, eachOfSourceIPValue.items()[0][1][3])
                            worksheet4.write(printRowDestinationOUTCounter, 25, eachOfSourceIPValue.items()[0][1][2])
                            printRowDestinationOUTCounter+=1
                            customIPOUTTrafficCompressCount+=1
                            customIPOUTTrafficCount += int(eachOfSourceIPValue.items()[0][1][2])

                            if tempDestinationIP not in dicOfOUTDestinationIP.keys():
                                dicOfOUTDestinationIP[tempDestinationIP] = [{keySourceIP:[tempDestinationPort,eachOfSourceIPValue.items()[0][1][0],1]}]
                                ##temp_dicOfINPorts[destinationPort] = 1
                            else:
                                uniqueOUTDestinationIPValue = True
                                for sourceIPMetaDeta in dicOfOUTDestinationIP[tempDestinationIP]:
                                    for keyOfSourceIPOnlyMetaDeta, valueOfSourceIPOnlyMetaDeta in sourceIPMetaDeta.items():
                                        if keyOfSourceIPOnlyMetaDeta == keySourceIP and tempDestinationPort == valueOfSourceIPOnlyMetaDeta[0] and eachOfSourceIPValue.items()[0][1][0] == valueOfSourceIPOnlyMetaDeta[1]:
                                            valueOfSourceIPOnlyMetaDeta[2] = valueOfSourceIPOnlyMetaDeta[2]+1
                                            uniqueOUTDestinationIPValue = False
                                if uniqueOUTDestinationIPValue == True:
                                    dicOfOUTDestinationIP[tempDestinationIP].append({keySourceIP:[tempDestinationPort,eachOfSourceIPValue.items()[0][1][0],1]})
                            ####worksheet4.write(printRowDestinationOUTCounter, 10, keySourceIP)

            if printRowDestinationINCounter > 26:
                tableStartPosition = "O25"
                tableEndPosition = "T" + str(printRowDestinationINCounter)
                worksheet4.add_table(tableStartPosition+':'+tableEndPosition, 
                    {'columns': [{'header': 'List of source IPs w/ IN traffic'},{'header': 'Port Numbers'},
                    {'header': 'Traffic Type'},{'header': 'Numbers of Connections'},{'header': 'DFW Rule ID'},{'header': 'Source HostName'},]})

            if printRowDestinationOUTCounter > 26:
                tableStartPosition = "V25"
                tableEndPosition = "AA" + str(printRowDestinationOUTCounter)
                worksheet4.add_table(tableStartPosition+':'+tableEndPosition, 
                    {'columns': [{'header': 'List of source IPs w/ OUT traffic'},{'header': 'Port Numbers'},
                    {'header': 'Traffic Type'},{'header': 'Numbers of Connections'},{'header': 'DFW Rule ID'},{'header': 'Source HostName'},]})


            #print "customIPINTrafficCount is:", customIPINTrafficCount
            #print "customIPOUTTrafficCount is:", customIPOUTTrafficCount

            #print "In traffic count is {}. Out Traffic count is {}".format(customIPINTrafficCount, customIPOUTTrafficCount)
            worksheet4.write(1, 0, "Total IN traffic on this IP")
            worksheet4.write(2, 0, "Total OUT traffic on this IP")

            worksheet4.write(1, 1, customIPINTrafficCount)
            worksheet4.write(2, 1, customIPOUTTrafficCount)


            inVsOutSizesChart = workbook.add_chart({'type': 'pie'})

            inVsOutSizesChart.add_series({
                'categories': '='+str(listOfIPtoSearch[0])+'!$A$2:$A$3',
                'values':     '='+str(listOfIPtoSearch[0])+'!$B$2:$B$3',
                'data_labels': {'percentage': True},
                'points': [
                    {'fill': {'color': 'green'}},
                    {'fill': {'color': 'red'}},
                ],
            })

            # Set Title of the pie chart
            inVsOutSizesChart.set_title({'name': 'IN vs OUT'})
            # Set size of the pie chart
            inVsOutSizesChart.set_size({'x_scale': 0.8, 'y_scale': 1})
            # Set an Excel chart style. Colors with white outline and shadow.
            inVsOutSizesChart.set_style(10)
            # Insert the chart into the worksheet.
            worksheet4.insert_chart('A5', inVsOutSizesChart, {'x_offset': 25, 'y_offset': 10})

            workbook.close()
        except:
            pass
    else:
        workbook.close()
        #sys.exit("End of Script.")

    return workbook

# --------------------------------------------------------- #
# Main function - calling dfw_vat function w/ log file name
# --------------------------------------------------------- #
if __name__ == '__main__':
    dfw_vat('dfwpktlogsVAT.log')
    print "Running as a script from terminal"
else:
    print "Runnind as a web app from Django"
