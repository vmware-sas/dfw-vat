# README #

This README documents steps that are necessary to get DFW-VATool up and running.


### What is this repository for? ###

* Quick summary: 
This read me file summarizes the configuration and function of NSX DFW Visualize and Analyze Tool. It also shows the step by step instructions on how to setup and run the script on an Ubuntu VM.

* Latest Version: 
1.0.2

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Requirements ###

Source machine - Ubuntu VM recommended 

```
#!

a. Python 2.7 installed
   i. Confirm python install by running command 'python' from cmd prompt/terminal 
  ii. If python not installed; Download and install python from https://www.python.org/downloads
b. Access to internet
c. Pip installed
   i. Confirm pip install by running command pip -V or pip --version
  ii. If pip not installed; run commands sudo apt-get upgrade 
       and sudo apt-get install python-pip
```


### How do I get set up? ###

```
#!
NOTE: This tool has dependency on only one external python library xlsxwriter, 
to create .xlsx file. In order to install the library, run the following command:
 o sudo pip install xlsxwriter
 o git clone https://thisispuneet@bitbucket.org/vmware-sas/dfw-vat.git
 o Rename you dfw pkt log file to ‘dfwpktlogs.log’ 
   and place it in 'vat-scripts/temp' folder. If 'temp' folder doesn't exist please create one.

NOTE: If you have multiple dfw pkt log files, place all the files in 'vat-scripts/temp' folder.
      DO NOT name any file as ‘dfwpktlogs.log’, instead run the 'consolidateLogs.py' script.
      See instructions below...
```

### If you have multiple log files ###

```
#!
If you have multiple log files first, you need to consolidate all log files into one log file.
You may do this by running 'consolidateLogs.py' python script.

a. Make sure you have placed all dfw pkt log files in 'vat-scripts/temp' folder.
b. From terminal, cd to vat-scripts folder.
b. Run the following python command:
    i. python consolidateLogs.py 
       o Script will pull all the .log files from 'vat-scripts/temp' folder.
       o It'll create a new empty log file name 'dfwpktlogsVAT.log' and copy contents 
         of all existing log files into this new 'dfwpktlogsVAT.log' file.
c. Once script finish, go to 'vat-scripts/temp' folder 
   and you’ll see a new log file name ‘dfwpktlogsVAT.log’.

Now you ready to run the main script. See instructions below...
```


### How to run the script? ###

```
#!
a. From terminal, cd to vat-scripts folder.
b. Run the following python command:
    i. python dfw-log-parser.py 
       o Script will open and validate dfwpktlogs.log file.
       o Input VM’s IP address: 
          (If you interested to generate report of a particular VM/App, 
           provide its IP address):
          a. Provide VM’s IP address (eg: 10.0.0.1).
          b. Or simply hit enter without providing the IP.
c. Once script finish, go to vat-scripts folder 
   and you’ll see a new excel file name ‘dfwpktlogsVAT.xlsx’.
```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines


### Who do I talk to? ###

* Repo owner & admin: Puneet Chawla 'cpuneet@vmware.com'